<?php

/* @author Mauro Pizzamiglio */
/* @version 0.4 */
/* (c) 2011 */
/*	TeddyBear HTTPClient in PHP: going down the layer */
/*	License: WTFPL */
/*
	TeddyBear is a simple HTTP client, which can sounds strange as long as PHP sits a layer upon.
	The main ideas behind TeddyBear are to learn something more about HTTP and to develop
	a solid platform to implements almost any API client for servicies (better a RESTful web service).
	
	TeddyBear would like to be strict HTTP 1.1 as possible:
		See features
		See TODO
*/
/*
(Un)Features
	+ manage gzipped responses
	+ no persistent connections
	+ POST multipart/form-data [data sent as chunked and base64 encode]
	+ cookie enable
	+ can read chunk responses!
*/

/*
TODO
	- handle the "100 Continue" response
	x better header manage to perform a lighter surf session (reset after a post some header etc etc)
	- debug method
	- utility method (switch gzip out, get request)
	/ better cookie manage -> now cookie array looks really nice!
	- mind cookie host / path and expiring
	- optionRequest auto add headers
*/

/*
	Fast [uncomplete] reference
	- <HTTP_METHOD_NAME>Request($RequestURI) : perform a HTTP_METHOD_NAME request
		if you do not pass the host within the constructor, $RequestURI *must* be absolute
		to find out and fill HOST header [as HTTP 1.1 request]
	
	- getHeader($HeaderName)
		get value of $HeaderName request Header, if $HeaderName is null return all the request Headers
	- setHeader($HeaderName, $value)
		set $HeaderName request header with $value value
	- unsetHeader($HeaderName)
		unset $HeaderName header
	- getResponse()
		after perform a request, get message body as a string [probably chunked issue here]
	- getResponseHeader($HeaderName)
		exactly like getHeader except this is about response header
	- getStatus()
		after perform a request this is the HTTP status code response
	- getCookie($CookieName)
		exactly like getHeader but it's about cookie. This will return cookie as an array,
		but you can still found cookie in the header array as a concatenate string
	- setCookie($CookieName, $value)
		set a cookie
	- getHTTPRequest()
		get the lates whole HTTP request;
	- simulate(boolean)
		if is set to true the bear will not really open a socket
		but just pack the request
*/

class TeddyBear {

	protected $header;
	protected $response;
	protected $responseHeader;
	protected $status = null;
	protected $basePath;
	protected $cookie = null;
	protected $simulate = false;
	
	// reader vars
	protected $socket;
	protected $isResponse = false;
	
	private $_lastchunkleft = 0;
	
	public $chunks = array();
	
	const HTTP_VERSION = "HTTP/1.1";
	const USER_AGENT = "TeddyBear0.4";
	
	const METHOD_OPTIONS = "OPTIONS";
	const METHOD_GET = "GET";
	const METHOD_HEAD = "HEAD";
	const METHOD_POST = "POST";
	const METHOD_PUT = "PUT";
	const METHOD_DELETE = "DELETE";
	const METHOD_TRACE = "TRACE";
	const METHOD_CONNECT = "CONNECT";
	
	const FORM_URLENCODED = "application/x-www-form-urlencoded";
	const FORM_MULTIPART = "multipart/form-data";
	
	/**
	 *	Constructor
	 *	@param	string	$Host	Base URI
	 *	@param	string $basePath Unused, for furture only
	 */
	public function TeddyBear($Host = null, $basePath = null/*, $persistent = false*/) {
		$this->setHeader("Connection", "close");
		if(!is_null($Host))
			$this->setHeader("Host", $Host);
		$this->basePath = $basePath;
		//$persistent = false;
		$persistent = 1;
		if($persistent)
			$this->setHeader("Connection", "keep-alive");
		$this->HTTP_version = self::HTTP_VERSION;
		$this->setHeader("User-Agent", self::USER_AGENT);
	}

	/**
	 *	Perform an HTTP Option request
	 *	
	 *	@param	string $URI Request URI
	 *	@param	mixed $data key/value array containing data request as query string
	 *	@param	string $entiti_body	HTTP Option request entity body
	 *	@return	boolean true or throw an error
	 */
	public function optionRequest($URI, $data = null, $entity_body = null) {
		if(!is_null($entity_body)) {
			if(is_null($this->getHeader("Content-Length")) || is_null($this->getHeader("Transfer-Encoding")))
				throw new \Exception("OPTION request with a message body must define Content-Length or Transfer-Encoding header");
			if(is_null($this->getHeader("Content-Type")))
				throw new \Exception("OPTION request with a message body must define media type with Content-Type header");
		}
		if(is_array($data))
			$URI .= "?" . http_build_query($data);
		return $this->_call(self::METHOD_OPTION, $URI, $entity_body);
	}
	
	/**
	 *	Perform an HTTP Get request
	 *	
	 *	@param	string $URI Request URI
	 *	@param	mixed $data key/value array containing data request as query string
	 *	@return	boolean true or throw an error
	 */
	public function getRequest($URI, $data = null) {
		if(is_array($data))
			$URI .= "?" . http_build_query($data);
		return $this->_call(self::METHOD_GET, $URI);
	}
	
	/**
	 *	Perform an HTTP Head request
	 *	
	 *	@param	string $URI Request URI
	 *	@param	mixed $data key/value array containing data request as query string
	 *	@return	boolean true or throw an error
	 */
	public function headRequest($URI, $data = null) {
		if(is_array($data))
			$URI .= "?" . http_build_query($data);
		return $this->_call(self::METHOD_HEAD, $URI);
	}
	
	/**
	 *	Perform an HTTP Option request
	 *	
	 *	@param	string $URI Request URI
	 *	@param	mixed $data key/value array containing data request as query string
	 *	@param	string $type	HTTP Post request type
	 *	@return	boolean true or throw an error
	 */
	public function postRequest($URI, $data = null, $type = self::FORM_URLENCODED) {
		if(is_array($data)) {
			switch($type) {
				case self::FORM_URLENCODED: {
					$post_data = http_build_query($data, '', "&");
					$this->setHeader("Content-Type", $type);
				}; break;
				case self::FORM_MULTIPART: {
					$boundary = md5(microtime());
					$this->setHeader("Content-Type", $type . "; boundary=" . $boundary);
					foreach($data as $name => $value) {
						$post_data .= "----" . $boundary . "\r\n";
						if(is_file($value)) {
							$post_data .= "Content-Disposition: form-data; name=\"$name\"; filename=\"$value\"\r\n";
							$post_data .= "Content-Type: " . $this->_get_MIME($value) . "\r\n";
							$post_data .= "Content-Transfer-Encoding: base64\r\n\r\n";
							$post_data .= chunk_split(base64_encode(file_get_contents($value))) . "\r\n";
						} else {
							$post_data .= "Content-Disposition: form-data; name=\"$name\"\r\n\r\n";
							$post_data .= "$value\r\n";
						}
					}
					$post_data .= "----" . $boundary . "--\r\n\r\n";
				}; break;
				default: { $post_data = null; };
			}
		}
		$this->setHeader("Content-Length", strlen($post_data));
		return $this->_call(self::METHOD_POST, $URI, $post_data);
	}

	/**
	 *	Get the response of a HTTP request
	 *	
	 *	@return	string Response of the latest request
	 */
	public function getResponse() {
		return $this->response;
	}

	/**
	 *	Get the response of a HTTP request
	 *	
	 *	@param	string $header This parameter specify a single header to be returned
	 *	@return	mixed Response Header of the latest response, or a single header if $header is not null
	 */
	public function getResponseHeader($header = null) {
		if(is_null($header)) {
			return $this->responseHeader;
		} else {
			return $this->responseHeader["$header"];
		}
	}

	/**
	 *	Get the header/s of a HTTP request
	 *	
	 *	@param	string $header This parameter specify a single header to be returned
	 *	@return	mixed Response Header of the latest request, or a single header if $header is not null
	 */
	public function getHeader($header = null) {
		if(is_null($header)) {
			return $this->header;
		} else {
			return $this->header["$header"];
		}
	}

	/**
	 *	Set a  header for a HTTP request
	 *	
	 *	@param	string $header This parameter specify header to set (eg: Accept)
	 *	@param	string $value The value of the header (eg: text/html,application/xhtml+xml,application/xml;)
	 */
	public function setHeader($header, $value) {
		$this->header["$header"] = $value;
	}

	/**
	 *	Unset the header/s of a HTTP request
	 *	
	 *	@param	string $header This parameter specify a single header to be returned
	 *	@return	mixed Response Header of the latest request, or a single header if $header is not null
	 */
	public function unsetHeader($header) {
		unset($this->header["$header"]);
	}

	/**
	 *	Set the headers of a HTTP request
	 *	
	 *	@param	mixed $header Array, key/value headers
	 */
	public function setHeaders($headers) {
		foreach($headers as $header => $value)
			$this->setHeader($header, $value);
	}

	/**
	 *	Get the latest HTTP Request status
	*
	 *	@return	string Response Header of the latest request, or a single header if $header is not null
	 */
	public function getStatus() {
		return $this->status;
	}

	/**
	 *	@todo
	 */
	public function setCookie($name, $value) {
		// REWRITE ME
		//$this->cookie["$name"] = $value;
		return true;
	}
	
	public function getCookie($cookie = null, $value = null) {
		if(is_null($cookie))
			return $this->cookie;
		elseif(is_null($value))
			return $this->cookie["$cookie"];
		else
			return $this->cookie["$cookie"]["$value"];
	}
	
	public function getHTTPRequest() {
		return $this->HTTP_request;
	}
	
	public function simulate($b) {
		$this->simulate = $b;
	}
	
	/**
	 *	This is the core, this method perform a request, putting together
	 *	header, entity body and HTTP method.
	 *	The connection run on a socket, this also split response into
	 *	HTTP Status, response headers and message body.
	 *	Reading response header inflate message body if it's gzipped.
	 *	Please note that POST data is handled before calling this
	 *	method.
	 */
	private function _call($method, $requestURI, $message_body = null) {
		$host = $this->_get_host($URI);
		if(is_null($message_body)) {
			$this->unsetHeader("Content-Length");
			$this->unsetHeader("Content-Type");
		}
		$this->HTTP_request = $this->_build_request($method, $requestURI, $message_body);
		if($this->simulate)
			return true;
		$this->socket = fsockopen($host, 80, $errno, $errstr, 30);
		stream_set_timeout($this->socket, 30);
		if(is_null($message_body))
			$fw = fwrite($this->socket, $this->HTTP_request);
		else
			$fw = $this->_fwrite_stream($this->socket, $this->HTTP_request);
		if($fw === FALSE)
			throw new \Exception("An error occur while writing to the socket.");

		if($this->socket) {
			while(!feof($this->socket))
				$this->_reader();
		} else {
			if($errno == 0) {
				throw new \Exception("The error occurred before the connect() call, most likely due to a problem initializing the socket. $errstr");
			} else {
				throw new \Exception("$errno: $errstr");
			}
		}
		fclose($this->socket);
		$message_body = $this->response;
		$this->_pack_response_header($header);
		if(isset($this->responseHeader["Content-Encoding"]) && $this->responseHeader["Content-Encoding"] == "gzip") {
			$message_body = gzinflate(substr($message_body, 10));
		}
		$this->response = $message_body;
		return true;
	}
	
	/**
	 *	Teddy's glasses!
	 *
	 *	@TODO make reader come back a value so
	 *	the bear can finally switch to a keep alive
	 *	connection.
	 */
	private function _reader($length = 4096) {
		$buffer = fread($this->socket, $length);
		if($this->isResponse) {
			if($this->getResponseHeader("Transfer-Encoding") == "chunked")
				$this->_unchunkdata($buffer);
			else
				$this->response .= $buffer;
		} else {
			$index = $this->_getHeader($buffer);
			if($this->getResponseHeader("Transfer-Encoding") == "chunked")
				$this->_unchunkdata(substr($buffer, $index));
			else
				$this->response .= substr($buffer, $index);
		}
	}
	
	/**
	 *	Teddy get mad to find a solution as clear as possible to unchunk
	 *	data likes HTTP loves to chunk!
	 *	Teddy take care that a buffer can contain more than one
	 *	chunk of data!
	 *	This is actually slow, but teddy is a little bear!
	 */
	private function _unchunkdata($buffer) {
		$matches = preg_match_all('/\r\n([A-Fa-f0-9]+)\r\n/', $buffer, $chunk);
		$chunkindex = 0;
		if($matches > 0) {
			for($i = 0; $i < $matches; $i++) {
				$plainchunk = $chunk[0][$i];
				$chunkindex = strpos($buffer, $plainchunk, $chunkindex);
				$chunkhex = $chunk[1][$i];
				$chunkdec = hexdec($chunkhex);
				// ok this is far from being elegant but it makes my day!
				$buffer = str_replace($plainchunk, "", $buffer);
				$this->_chunkleft = $chunkdec - strlen(substr($buffer, $chunkindex));
				if(($this->_chunkleft - $chunkindex) == 0) {
					$this->response .= trim(substr($buffer, 0, $chunkindex));
				} elseif($chunkdec < strlen(substr($buffer, $chunkindex))) {
					$this->response .= trim(substr($buffer, $chunkindex, $chunkdec));
				} else {
					$this->response .= $buffer;
				}
			}
		} else {
			$this->_chunkleft -= strlen($buffer);
			$this->response .= $buffer;
		}
	}
	
	/**
	 *	Teddy use this function to keep clean the code
	 *	and let him understand in a recursion or in a while
	 *	loop like he actually do, to recon if the
	 *	current buffer is header or response.
	 */
	private function _getHeader($buffer) {
		$index = strpos($buffer, "\r\n\r\n");
		$this->isResponse = true;
		$this->_pack_response_header(substr($buffer, 0, $index));
		return $index;
	}
	
	/**
	 *	In order to be sure that the Host header will be send
	 *	within every request, this method check if it is
	 *	already defined or exctract it from the URI.
	 *	The host *should* be defined within the constructor
	 *	on the other hand, this method extract the host from
	 *	the absolute URI request.
	 */
	private function _get_host($URI) {
		$host = $this->getHeader("Host");
		if(is_null($host)) {
			$host = parse_url($URI, PHP_URL_HOST);
			$this->setHeader("Host", $host);
		}
		return $host;
	}
	
	/**
	 *	Putting together a request, please note
	 *	that the entity body is already "processed"
	 *	in the specific public method called to perform
	 *	the request. This method just concatenate necessary
	 *	elements.
	 *
	 *	@see _build_headers()
	 */
	private function _build_request($method, $requestURI, $entity_body = null) {
		return $method . " " .  $requestURI . " " . self::HTTP_VERSION . "\r\n" . $this->_build_headers() . "\r\n" . $entity_body . "\r\n\r\n";
	}
	
	/**
	 *	In order to be sure the cases HTTP 1.1 requests
	 *	that the client close connection in case of error
	 *	during sending data to a server, this method
	 *	assure to fill the request.
	 *	
	 */
	private function  _fwrite_stream($fp, $string) {
		for ($written = 0; $written < strlen($string); $written += $fwrite) {
			$fwrite = fwrite($fp, substr($string, $written));
			if ($fwrite === false) {
				fclose($fp);
				throw new Exception("An error occur while writing to the socket");
			}
		}
		return $written;
	}
	
	/**
	 *	This method is liable to build  the request header,
	 *	as long as TeddyBear treat cookie as an array,
	 *	cookie will be replicated (obviously) as a concatenated
	 *	string in the header.
	 */
	private function _build_headers() {
		$headers = "";
		foreach($this->header as $name => $value)
			$headers .= "$name: $value\r\n";
		if(!is_null($this->cookie)) {
			$cookie = "Cookie: ";
			foreach($this->cookie as $k => $v)
				$cookie .= $this->_cookie_implode($k) . ", ";
		}
		$headers .= substr($cookie, 0, -2) . "\r\n";
		return $headers;
	}
	
	/**
	 *	TeddyBear uses socket to estabilish connections and to
	 *	communicate with servers, so the responses come back
	 *	and put into a string.
	 *	This method split the header section into
	 *	a more comfortable array.
	 */
	private function _pack_response_header($header) {
		$tmp = explode("\r\n", $header);
		list($this->protoResponse, $this->status, $a) = explode(" ", array_shift($tmp));
		unset($a);
		foreach($tmp as $h) {
			list($k, $v) = explode(":", $h, 2);
			if($k == "Set-Cookie") {
				$this->_cookie_explode($v);
			} else {
				$this->responseHeader["$k"] = trim($v);
			}
		}
	}
	
	/**
	 *	HTTP 1.1 POST method as multipart/form-data, requests
	 *	you specify file's MIME type you are sending to the
	 *	server. TeddyBear don't want to take effort
	 *	to recognize MIME type, so finfo is what the bear is
	 *	looking for...
	 */
	private function _get_MIME($file) {
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		$MIME = finfo_file($finfo, $file);
		finfo_close($finfo);
		return $MIME;
	}
	
	/**
	 *	The bear Teddy loves nice array,
	 *	this method help Teddy to keep clean cookie;
	 *	teddy loves to read rfc!
	 */
	private function _cookie_explode($cookie) {
		preg_match_all('/([^;=]+)(?:\s?=\s?([^;]+))?[;\s]+/', $cookie, $tmp);
		$name = trim($tmp[1][0]);
		$value = $tmp[2][0];
		array_shift($tmp[1]);
		array_shift($tmp[2]);
		$this->cookie["$name"]["value"] = $value;
		foreach($tmp[1] as $index => $k)
			$this->cookie["$name"]["$k"] = $tmp[2][$index];
	}
	
	private function _cookie_implode($cookie) {
		$str = "$cookie=".$this->getCookie($cookie, "value");
		foreach($this->cookie["$cookie"] as $attr => $value)
			if($attr == "value")
				continue;
			else
				$str .= "; $attr=$value";
		return $str;
	}
}
/*
$tb = new TeddyBear("fa.wikipedia.org");
$tb->setHeader("Connection", "close");
$tb->getRequest("/wiki/%D8%B5%D9%81%D8%AD%D9%87%D9%94_%D8%A7%D8%B5%D9%84%DB%8C");
echo $tb->getResponse();
*/
/*
// chunked
$tb = new TeddyBear("php.net");
$tb->setHeader("Connection", "close");
$tb->getRequest("/");
echo $tb->getResponse();
*/
